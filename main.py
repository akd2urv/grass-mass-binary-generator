from random import randint
import random
from material_buffer import MaterialBuffer
from physics_lambda import generate_physics_lambda
from ring_placer import RingPlacer

#Establishes material buffer and ring placer
material_buffer = MaterialBuffer(201,201,1)
ring_placer = RingPlacer(material_buffer)

#Slightly variable inner radius
ring_placer.ring_inner_lambda = lambda mb,n: (random.normalvariate(12.5,5)) 
#Slightly variable outer radius
ring_placer.ring_outer_lambda = lambda mb,n,in_r: (in_r+random.uniform(1.5,3))
#Grid pattern placement with slight divergence
ring_placer.ring_placement_lambda = generate_physics_lambda(2,0.1,0.1,True,0,0)
#5% chance that a random grass pixel will not place, simulating inconsistencies in biological material
ring_placer.placement_bool_lambda = lambda mb,ring,x,y: (randint(0,10)!=0)
#0.1% chance that a random grass pulp pixel will not place, low porosity
ring_placer.inner_placement_bool_lambda = lambda mb,ring,x,y: (randint(0,5)!=0)

#Performs ring calculations
ring_placer.generate_rings(150,2,3)

#Saves material buffer as text
ring_placer.save_text('material.txt')
#Saves material buffer with empty space being white and filled space being black
ring_placer.save_image('material.png',{1:(255,255,255),2:(0,0,0),3:(140,140,80)})