#Defined class for output "image" representing material binaries
class MaterialBuffer:
    def __init__(self,width: int,height: int,default_material: int) -> int:
        self.width = width
        self.height = height
        self.default_material = default_material
        if not(default_material):
            default_material = 0

        #C might be more suitable for this purpose
        self.arr = [ [default_material]*width for _ in range(height)]
        
    #Sets the material at a given point
    def set_material(self,x:int ,y:int,material:int) -> int:
        self.arr[y][x] = material

    #Gets the material at a given point
    def get_material(self,x:int,y:int) -> int:
        return self.arr[y][x]

    def __str__(self):
        out = ''
        for index, row in enumerate(self.arr):
            out = out + format(index+1,'<5d') + '   ' + ' '.join(str(x) for x in row) + '\n'

        return out
    
#Uncomment for test (unit tests nonwithstanding)
#mb = MaterialBuffer(2,3)
#mb.set_material(1,1,2)
#mb.set_material(0,1,3)
#print(str(mb))