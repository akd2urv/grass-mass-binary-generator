from math import copysign, sqrt
import random
from typing import List, Tuple
from material_buffer import MaterialBuffer
from ring import Ring

def mag_sq(vec):
    return vec[0]*vec[0]+vec[1]*vec[1]

def mag(vec):
    return sqrt(mag_sq(vec))



#Makes the rings fall in a physical fashion
def physics_lambda(
    mb: MaterialBuffer,
    n: int, rings: List[Ring],
    in_r: float,
    out_r: float,
    ghost_radius_multiplier: float,
    dxdn: float = 0.1,
    dydn: float = 0.1,
    do_out_box_x: bool = False,
    outbox_x: float = 4,
    outbox_y: float = 4) -> Tuple[int,int]:

    ghost_radius = out_r * ghost_radius_multiplier
    print(n)
    [px,py] = (random.normalvariate(mb.width/2,out_r*15),-300)

    terminate = 300

    t = 0

    past_ring = None
    #Until it hits the bottom or sooner
    while py+out_r+1 < mb.height+outbox_y*out_r and (not(do_out_box_x) or (px-out_r > - outbox_x*out_r and px+out_r < mb.width+outbox_x*out_r)) and  t<terminate:
        

        collide_ring = None
        #Checks if there is a ring below
        for ring in rings:
            if(ring.intersects_ring(px,py,ghost_radius)):
                collide_ring = ring
                break
        
        if collide_ring:
            dx = px - collide_ring.x
            #To prevent standing grass stalks from never completing
            if dx ==0: break
            while ring.intersects_ring(px,py+dydn,ghost_radius):
                px += copysign(dxdn,dx)
            if collide_ring != past_ring:
                t += 1 
        else:
            py += dydn

    
    return (px,py)

def generate_physics_lambda(ghost_radius_multiplier:float, dxdn: float, dydn: float, do_out_box_x: bool, outbox_x: float, outbox_y: float):
    def out_function(mb: MaterialBuffer,n: int, rings: List[Ring],in_r: float,out_r: float):
        return physics_lambda(mb,n,rings,in_r,out_r,ghost_radius_multiplier,dxdn,dydn,do_out_box_x,outbox_x,outbox_y)
    return out_function