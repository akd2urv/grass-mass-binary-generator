from typing import Callable, List, Tuple
from PIL import Image
from material_buffer import MaterialBuffer
from ring import Ring

class RingPlacer:
    def __init__(self,material_buffer: MaterialBuffer):
        self.material_buffer = material_buffer
        self.width = self.material_buffer.width
        self.height = self.material_buffer.height

        self.ring_list = []

        #Initializes default lambda placements
        
        #Lambda function representing ring inner radius given n-th ring in material buffer mb, and center variables x and y
        #Returns scalar representing ring inner radius
        self.ring_inner_lambda: Callable[[MaterialBuffer,int],float]= lambda mb, n: 1
        
        #Lambda function representing ring inner radius given n-th ring in material buffer mb, and center variables x and y
        #Returns scalar representing ring outer radius
        self.ring_outer_lambda: Callable[[MaterialBuffer,int,int],float] = lambda mb, n, in_r: 2

        #Lambda function representing ring center placement given n-th ring in material buffer mb, given inner radius of ring and outer radius
        #Returns ordered pair representing center position
        self.ring_placement_lambda: Callable[[MaterialBuffer,int,List[Ring],float,float],Tuple[int,int]] = lambda mb, n, rings, inner_r, outer_r: (6*(n%2),6*int(n/2))

        #Lambda function representing whether or not a single "pixel" of ring will show up or not: given n-th ring in material buffer mb,
        #ring object ring, and x and y value of pixel
        #Returns boolean value representing whether or not the given pixel has its material changed
        self.placement_bool_lambda: Callable[[MaterialBuffer,Ring,float,float],bool] = lambda mb, ring, x, y: True

        #Lambda function representing whether or not a single "pixel" of pulp inside ring will appear or not, given n-th ring in material buffer mb,
        #ring object ring, and x and y value of pixel
        #Returns boolean value representing whether or not the given pixel has its material changed
        self.inner_placement_bool_lambda: Callable[[MaterialBuffer,Ring,float,float],bool] = lambda mb, ring, x, y: True

    #Generates rings according to how the user defined them previously.
    def generate_rings(self, n_tot: int, sheath_material: int, pulp_material: int) -> None:
        for n in range(n_tot):
            ring_in_r = self.ring_inner_lambda(self.material_buffer, n)
            ring_out_r = self.ring_outer_lambda(self.material_buffer, n, ring_in_r)
            [ring_x,ring_y] = self.ring_placement_lambda(self.material_buffer, n, self.ring_list, ring_in_r, ring_out_r)

            ring = Ring(ring_x,ring_y,ring_in_r,ring_out_r)
            
            self.ring_list.append(ring)

            #Establishes limits for loop
            min_x = max(0,int(ring_x-ring_out_r-1))
            max_x = min(self.width,int(ring_x+ring_out_r+1))
            min_y = max(0,int(ring_y-ring_out_r-1))
            max_y = min(self.height,int(ring_y+ring_out_r+1))

            for x in range(min_x,max_x):
                for y in range(min_y,max_y):
                    if(ring.is_on_ring(x,y)):
                        #Ensures that the square is empty
                        if self.material_buffer.get_material(x,y) == self.material_buffer.default_material:
                            #Does check specific to individual ring placer lambda
                            is_valid = self.placement_bool_lambda(self.material_buffer,ring,x,y)
                            if is_valid:
                                #Places material down at x,y
                                self.material_buffer.set_material(x,y,sheath_material)
                    elif ring.is_inside_ring(x,y):
                            is_valid = self.inner_placement_bool_lambda(self.material_buffer,ring,x,y)
                            if is_valid:
                                #Places material down at x,y
                                self.material_buffer.set_material(x,y,pulp_material)
    
    #Saves material buffer text to a file
    def save_text(self,filename: str) -> None:
        with open(filename,'w') as file:
            file.write(str(self.material_buffer))

    #Saves material buffer as an image, with each different material's pixel color being defined in the color_map dictionary
    def save_image(self,filename: str,color_map: dict[int,str]) -> None:
        img = Image.new('RGB',[self.width,self.height], color='white')
        pixels = img.load()

        for x in range(self.width):
            for y in range(self.height):
                material = self.material_buffer.get_material(x,y)
                pixels[x,y] = color_map[material]
        
        img.save(filename)